---
title: "Query string hash"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: security 
date: "2016-09-06"
---

# Query string hash

A query string hash (QSH) is an important technique to prevent URL tampering
and secure HTTP requests when they are made via a browser.
The QSH secures the HTTP method,
the relative URI path,
and the query string parameters.
A QSH claim is a SHA-256 hash of a [canonical request](#canonical-request),
sent within a [JWT token](/cloud/bitbucket/understanding-jwt-for-apps) as a custom claim.
Atlassian products send QSH claims
that **must** be validated by apps to prevent leaking sensitive data.
Apps **must** send QSH claims within JWT tokens
when making HTTP requests on Atlassian products.
When an app makes a requests without a QSH claim,
or with a bad QSH claim (one that doesn't match the request),
then the Atlassian product will respond with HTTP status `401 Unauthorized`.
Likewise, apps that detect a bad QSH claim should respond
with HTTP status `401 Unauthorized`.

## Canonical request

A canonical request is a normalized representation of the HTTP request.
It consists of:

* [Canonical method](#canonical-method)
* [Canonical URI](#canonical-uri)
* [Canonical query string](#canonical-query-string)

These elements are joined by ampersand.

**Example**

<table>
    <thead>
        <tr>
            <th>Canonical method</th>
            <th>Canonical URI</th>
            <th>Canonical query string</th>
            <th>Canonical request</th>
        </tr>
    </thead>
    <tr>
        <td><code>GET</code></td>
        <td><code>/</code></td>
        <td><code>param=foo</code></td>
        <td><code>GET&amp;/&amp;param=foo</code></td>
    </tr>
    <tr>
        <td><code>POST</code></td>
        <td><code>/user</code></td>
        <td> </td>
        <td><code>POST&/user&</code></td>
    </tr>
</table>

## Canonical method

The canonical method is the [HTTP method](https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html) in upper case.

**Example**

<table>
    <thead>
        <tr>
            <th>HTTP method</th>
            <th>Canonical method</th>
        </tr>
    </thead>
    <tr>
        <td><code>get</code></td>
        <td><code>GET</code></td>
    </tr>
    <tr>
        <td><code>Get</code></td>
        <td><code>GET</code></td>
    </tr>
    <tr>
        <td><code>GET</code></td>
        <td><code>GET</code></td>
    </tr>
    <tr>
        <td><code>POST</code></td>
        <td><code>POST</code></td>
    </tr>
</table>


[http-method]: https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html "Hypertext Transfer Protocol -- HTTP/1.1: Method Definitions"

## Canonical URI

The canonical URI is the relative URI path for the Atlassian product or app,
that represents a specific resource for the request.
As a relative URI path,
the canonical URI can survive URL rewrites performed by a reverse proxy.
Therefore the canonical URI discards
the protocol, server, port, context path, and query parameters from the request URL.
The canonical URI always starts with a `/` character.
Therefore, empty-string is not permitted
and the minimum canonical URI is just `/`.
The canonical URI only ends with a `/` character if it is the only character.
The canonical URI should not contain `&` characters.
Therefore, any `&` characters should be URL-encoded to `%26`.

For requests targeting apps,
Atlassian products will discard the `baseUrl` in the app descriptor.
For requests targeting Atlassian products,
apps discard the `baseUrl` sent in the lifecycle installed callback.

**Example**

<table>
    <thead>
        <tr>
            <th>Base URL</th>
            <th>Request URL</th>
            <th>Canonical URI</th>
        </tr>
    </thead>
    <tr>
        <td><code>https://addon.example.com/jira-connector</code></td>
        <td><code>https://addon.example.com/jira-connector</code></td>
        <td><code>/</code></td>
    </tr>
    <tr>
        <td><code>https://addon.example.com/jira-connector</code></td>
        <td><code>https://addon.example.com/jira-connector/issue</code></td>
        <td><code>/issue</code></td>
    </tr>
    <tr>
        <td><code>https://addon.example.com/jira-connector</code></td>
        <td><code>https://addon.example.com/jira-connector/title&description</code></td>
        <td><code>/title%26description</code></td>
    </tr>
    <tr>
        <td ><code>https://example.atlassian.net/</code></td>
        <td><code>https://example.atlassian.net/rest/api/2/issue/</code></td>
        <td><code>/rest/api/2/issue</code></td>
    </tr>
</table>

## Canonical query string

The canonical query string is a normalization of the query parameters,
that accounts for the denormalization of query parameters.
In other words, query strings
may have parameters in any order,
may have the same parameter keys multiple times,
or stray characters that have no semantic meaning.
Because of this,
the same parameter meaning can be expressed in many different query strings.
The canonical query string factors out the differences
so the queries can be compared.
The rules for normalization follow.

### Ignore the JWT parameter

**Example**

<table>
    <thead>
        <tr>
            <th>Query string</th>
            <th>Canonical query string</th>
        </tr>
    </thead>
    <tr>
        <td><code>jwt=ABC.DEF.GHI</code></td>
        <td> </td>
    </tr>
    <tr>
        <td><code>expand=names&jwt=ABC.DEF.GHI</code></td>
        <td><code>expand=names</code></td>
    </tr>
</table>

### URL-encode parameter keys

**Example**

<table>
    <thead>
        <tr>
            <th>Query string</th>
            <th>Canonical query string</th>
        </tr>
    </thead>
    <tr>
        <td><code>enabled</code></td>
        <td><code>enabled</code></td>
    </tr>
    <tr>
        <td><code>some+spaces+in+this+parameter</code></td>
        <td><code>some%20spaces%20in%20this%20parameter</code></td>
    </tr>
    <tr>
        <td><code>connect*</code></td>
        <td><code>connect%2A</code></td>
    </tr>
    <tr>
        <td><code>1+%2B+1+equals+3</code></td>
        <td><code>1%20%2B%201%20equals%203</code></td>
    </tr>
    <tr>
        <td><code>in+%7E3+days</code></td>
        <td><code>in%20~3%20days</code></td>
    </tr>
</table>

### URL-encode parameter values

For each parameter concatenate its URL-encoded name
and its URL-encoded value
with the `=` character.

**Example**

<table>
    <thead>
        <tr>
            <th>Query string</th>
            <th>Canonical query string</th>
        </tr>
    </thead>
    <tr>
        <td><code>param=value</code></td>
        <td><code>param=value</code></td>
    </tr>
    <tr>
        <td><code>param=some+spaces+in+this+parameter</code></td>
        <td><code>param=some%20spaces%20in%20this%20parameter</code></td>
    </tr>
    <tr>
        <td><code>query=connect*</code></td>
        <td><code>query=connect%2A</code></td>
    </tr>
    <tr>
        <td><code>a=b&</code></td>
        <td><code>a=b</code></td>
    </tr>
    <tr>
        <td><code>director=%E5%AE%AE%E5%B4%8E%20%E9%A7%BF</code></td>
        <td><code>director=%E5%AE%AE%E5%B4%8E%20%E9%A7%BF</code></td>
    </tr>
</table>

### URL-encoding is upper case

URL-encoded characters in query string hashes should be upper case,
like `%2A`, not `%2a`.

**Example**

<table>
    <thead>
        <tr>
            <th>Query string</th>
            <th>Canonical query string</th>
        </tr>
    </thead>
    <tr>
        <td><code>director=%e5%ae%ae%e5%b4%8e%20%e9%a7%bf</code></td>
        <td><code>director=%E5%AE%AE%E5%B4%8E%20%E9%A7%BF</code></td>
    </tr>
</table>

### Sort query parameter keys

**Example**

<table>
    <thead>
        <tr>
            <th>Query string</th>
            <th>Canonical query string</th>
        </tr>
    </thead>
    <tr>
        <td><code>a=x&b=y</code></td>
        <td><code>a=x&b=y</code></td>
    </tr>
    <tr>
        <td><code>a10=1&a1=2&b1=3&b10=4</code></td>
        <td><code>a1=2&a10=1&b1=3&b10=4</code></td>
    </tr>
    <tr>
        <td><code>=A&a=a&b=b&B=B</code></td>
        <td><code>A=A&B=B&a=a&b=b</code></td>
    </tr>
</table>

**Example**

Using a complex Jira request, a query like this:

``` javascript
link=http%3A%2F%2Fion%3A2990%2Fjira%2Fsecure%2FIssueNavigator.jspa%3Freset%3Dtrue%26jqlQuery%3Dissuetype%2B%253D%2BBug&
startIssue=0&
totalIssues=2&
endIssue=2&
issues=issues%3DTEST-2%2CTEST-1&
tz=Australia%2FSydney&
loc=en-US&
user_id=admin&
user_key=admin&
xdm_e=http%3A%2F%2Fion.local%3A2990&
xdm_c=channel-acmodule-1564427223927602208&
cp=jira&
lic=none
```

Will be sorted like this:

``` javascript
cp=jira&
endIssue=2&
issues=issues%3DTEST-2%2CTEST-1&
lic=none&
link=http%3A%2F%2Fion%3A2990%2Fjira%2Fsecure%2FIssueNavigator.jspa%3Freset%3Dtrue%26jqlQuery%3Dissuetype%2B%253D%2BBug&
loc=en-US&
startIssue=0&
totalIssues=2&
tz=Australia%2FSydney&
user_id=admin&
user_key=admin&
xdm_c=channel-acmodule-1564427223927602208&
xdm_e=http%3A%2F%2Fion.local%3A2990
```

### Sort query parameter value lists

In the case of repeated parameters, concatenate sorted values with a `,` character.

**Example**

<table>
    <thead>
        <tr>
            <th>Query string</th>
            <th>Canonical query string</th>
        </tr>
    </thead>
    <tr>
        <td><code>ids=-1&ids=1&ids=10&ids=2&ids=20</code></td>
        <td><code>ids=-1,1,10,2,20</code></td>
    </tr>
    <tr>
        <td><code>ids=.1&ids=.2&ids=%3A1&ids=%3A2</code></td>
        <td><code>ids=.1,.2,%3A1,%3A2</code></td>
    </tr>
    <tr>
        <td><code>ids=10%2C2%2C20%2C1</code></td>
        <td><code>ids=10%2C2%2C20%2C1</code></td>
    </tr>
    <tr>
        <td><code>tuples=1%2C2%2C3&tuples=6%2C5%2C4&tuples=7%2C9%2C8</code></td>
        <td><code>tuples=1%2C2%2C3,6%2C5%2C4,7%2C9%2C8</code></td>
    </tr>
    <tr>
        <td><code>chars=%E5%AE%AE&chars=%E5%B4%8E&chars=%E9%A7%BF</code></td>
        <td><code>chars=%E5%AE%AE,%E5%B4%8E,%E9%A7%BF</code></td>
    </tr>
    <tr>
        <td><code>c=&c=+&c=%2520&c=%2B</code></td>
        <td><code>c=,%20,%2520,%2B</code></td>
    </tr>
    <tr>
        <td><code>a=x1&a=x10&b=y1&b=y10</code></td>
        <td><code>a=x1,x10&b=y1,y10</code></td>
    </tr>
    <tr>
        <td><code>a=another+one&a=one+string&b=and+yet+more&b=more+here</code></td>
        <td><code>a=another%20one,one%20string&b=and%20yet%20more,more%20here</code></td>
    </tr>
    <tr>
        <td><code>a=1%2C2%2C3&a=4%2C5%2C6&b=a%2Cb%2Cc&b=d%2Ce%2Cf</code></td>
        <td><code>a=1%2C2%2C3,4%2C5%2C6&b=a%2Cb%2Cc,d%2Ce%2Cf</code></td>
    </tr>
</table>


## QSH claim

The QSH claim is a SHA-256 hash of the canonical request as UTF-8 bytes.
The hash is hex encoded with lowercase `a`-`f` characters.
The claim is sent inside the JWT token body
so it is eventually signed with the shared secret.

**Example**

<table>
    <thead>
        <tr>
            <th>Canonical request</th>
            <th>QSH claim</th>
        </tr>
    </thead>
    <tr>
        <td><code>GET&/test&param=value</code></td>
        <td><code>be16910858a41fd19ea5c1b4e9decca9a784d1024cb00b2158defe2f29dc86dd</code></td>
    </tr>
    <tr>
        <td><code>POST&/rest/api/2/issue&</code></td>
        <td><code>43dd1779e33c34fae00c308d62e5dd153a32147d1bcb5d40b3936457fda0ece4</code></td>
    </tr>
</table>

