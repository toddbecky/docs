---
title: "Inline"
platform: cloud
product: bitbucketcloud
category: reference
subcategory: javascript 
date: "2017-05-18"
---

# Messages

Messages are the primary method for providing system feedback in the product user interface.
Messages include notifications of various kinds: alerts, confirmations, notices, warnings, info and errors.

For visual examples of each kind please see the [Design guide](https://developer.atlassian.com/design/latest/communicators/messages/).

### Example
``` javascript
AP.require(&quot;messages&quot;, function(messages){
  //create a message
  var message = messages.info('plain text title', 'plain text body');
});
```
 
## Classes

### MessageOptions

#### Properties

<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Type</th>
        <th>Description</th>
    </tr>
    </thead>
    <tbody>
        <tr>
                <td><code>closeable</code></td>
            <td>Boolean</td>
            <td>Adds a control allowing the user to close the message, removing it from the page</td>
        </tr>
        <tr>
                <td><code>fadeout</code></td>
            <td>Boolean</td>
            <td>Toggles the fade away on the message</td>
        </tr>
        <tr>
                <td><code>delay</code></td>
            <td>      
<span>Number</span>
            </td>
            <td>Time to wait (in ms) before starting fadeout animation (ignored if fadeout==false)</td>
        </tr>
        <tr>
                <td><code>duration</code></td>
            <td>Number</td>
            <td>Fadeout animation duration in milliseconds (ignored if fadeout==false)</td>
        </tr>
    </tbody>
</table>

## Methods

### clear (id)

Clear a message.

#### Parameters 

<table>
    <thead>
	<tr>
		<th>Name</th>
		<th>Type</th>
		<th>Description</th>
	</tr>
	</thead>
	<tbody>
        <tr>
                <td><code>id</code></td>
            <td>String</td>
            <td>The ID that was returned when the message was created</td>
        </tr>
	</tbody>
</table>

#### Example
        
``` javascript
AP.require("messages", function(messages){
  //create a message
  var message = messages.info('title', 'body');
  setTimeout(function(){
    messages.clear(message);
  }, 2000);
});
```
### error (title, body, options)

Show an error message.

#### Parameters

<table>
    <thead>
	<tr>
		<th>Name</th>
		<th>Type</th>

		<th>Description</th>
	</tr>
	</thead>
	<tbody>
        <tr>
                <td><code>title</code></td>
            <td>String</td>
            <td>Sets the title text of the message</td>
        </tr>
        <tr>
                <td><code>body</code></td>
            <td>String</td>
            <td>The main content of the message</td>
        </tr>
        <tr>
                <td><code>options</code></td>
            <td>MessageOptions</td>
            <td>Message Options</td>
        </tr>
	</tbody>
</table>

**Returns**: the ID to be used when clearing the message; String.

#### Example
        
``` javascript
AP.require("messages", function(messages){
  //create a message
  var message = messages.error('title', 'error message example');
});
```

### generic (title, body, options)

Show a generic message.

#### Parameters 

<table>
    <thead>
	<tr>

		<th>Name</th>
		<th>Type</th>
		<th>Description</th>
	</tr>
	</thead>
	<tbody>
        <tr>
                <td><code>title</code></td>
            <td>    
<span>String</span> 
            </td>
            <td>Sets the title text of the message</td>
        </tr>
        <tr>
                <td><code>body</code></td>
            <td>String</td>

            <td>The main content of the message</td>
        </tr>
        <tr>
                <td><code>options</code></td>
            <td>MessageOptions</td>

            <td>Message Options</td>
        </tr>
	</tbody>
</table>

**Returns**: the ID to be used when clearing the message; String.

#### Example

``` javascript
AP.require("messages", function(messages){
  //create a message
  var message = messages.generic('title', 'generic message example');
});
```

### hint (title, body, options) &rarr; {String}

Show a hint message. 
    
#### Parameters    

<table>
    <thead>
	<tr>
		<th>Name</th>
		<th>Type</th>
		<th>Description</th>
	</tr>
	</thead>
	<tbody>
        <tr>
                <td><code>title</code></td>
            <td>String</td>

            <td>Sets the title text of the message</td>
        </tr>
        <tr>
                <td><code>body</code></td>

            <td>String</td>
            <td>The main content of the message</td>
        </tr>
        <tr>
                <td><code>options</code></td>
            <td>     
<span>MessageOptions</span>
            </td>
            <td>Message Options</td>
        </tr>
	</tbody>
</table>

**Returns**: the ID to be used when clearing the message; String.

#### Example
        
``` javascript
AP.require("messages", function(messages){
  //create a message
  var message = messages.hint('title', 'hint message example');
});
```

### info (title, body, options) &rarr; {String}

Show an info message.

#### Parameters

<table>
    <thead>
	<tr>
		<th>Name</th>
		<th>Type</th>
		<th>Description</th>
	</tr>
	</thead>
	<tbody>
        <tr>
                <td><code>title</code></td>
            <td>String</td>

            <td>Sets the title text of the message</td>
        </tr>
        <tr>
                <td><code>body</code></td>
            <td>String</td>
            <td>The main content of the message</td>
        </tr>
        <tr>
                <td><code>options</code></td>
            <td>MessageOptions</td>
            <td>Message Options</td>
        </tr>

	</tbody>
</table>

**Returns**: the ID to be used when clearing the message; String.

#### Example
        
``` javascript
AP.require("messages", function(messages){
  //create a message
  var message = messages.info('title', 'info message example');
});
```

### success (title, body, options)

Show a success message.

#### Parameters

<table>
    <thead>
	<tr>
		<th>Name</th>
		<th>Type</th>
		<th>Description</th>
	</tr>
	</thead>
	<tbody>
        <tr>
                <td><code>title</code></td>
            <td>String</td>
            <td>Sets the title text of the message</td>
        </tr>
        <tr>
                <td><code>body</code></td>
            <td>String</td>
            <td>The main content of the message</td>
        </tr>
        <tr>
                <td><code>options</code></td>
            <td>MessageOptions</td>
            <td>Message Options</td>
        </tr>
	</tbody>
</table>

**Returns**: the ID to be used when clearing the message; String.

#### Example

``` javascript
AP.require("messages", function(messages){
  //create a message
  var message = messages.success('title', 'success message example');
});
```
### warning (title, body, options) &rarr; {String}

Show a warning message.

#### Parameters

<table>
    <thead>
	<tr>
		<th>Name</th>
		<th>Type</th>
		<th>Description</th>
	</tr>
	</thead>
	<tbody>
        <tr>
                <td><code>title</code></td>
            <td>String</td>
            <td>Sets the title text of the message</td>
        </tr>
        <tr>
                <td><code>body</code></td>
            <td>String</td>
            <td>The main content of the message</td>
        </tr>
        <tr>
                <td><code>options</code></td>
            <td>MessageOptions</td>
            <td class="description last"><p>Message Options</p></td>
        </tr>
	</tbody>
</table>

**Returns**: the ID to be used when clearing the message; String.

#### Example
        
``` javascript
AP.require("messages", function(messages){
  //create a message
  var message = messages.warning('title', 'warning message example');
});
```