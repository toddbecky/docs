---
title: Storing data without a database
platform: cloud
product: jsdcloud
category: devguide
subcategory: learning
date: "2017-08-25"
aliases:
- /cloud/jira/service-desk/storing-data-without-a-database.html
- /cloud/jira/service-desk/storing-data-without-a-database.md
---
{{< include path="docs/content/cloud/connect/concepts/storing-jira-data-without-a-database.snippet.md">}}