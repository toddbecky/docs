---
title: "OAuth 2.0 - JWT Bearer token authorization grant type"
platform: cloud
product: jsdcloud
category: devguide
subcategory: security
date: "2017-08-25"
---
{{< include path="docs/content/cloud/connect/concepts/oauth2-jwt-bearer-token-authentication.snippet.md">}}