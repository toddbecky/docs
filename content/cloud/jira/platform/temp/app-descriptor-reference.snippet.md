## App descriptor structure

``` json
{
  "modules": {},
  "key": "my-app-key",
  "name": "My Connect App",
  "description": "A connect app that does something",
  "vendor": {
    "name": "My Company",
    "url": "http://www.example.com"
  },
  "links": {
    "self": "http://www.example.com/connect/jira"
  },
  "lifecycle": {
    "installed": "/installed",
    "uninstalled": "/uninstalled"
  },
  "baseUrl": "http://www.example.com/connect/jira",
  "authentication": {
    "type": "jwt"
  },
  "enableLicensing": true,
  "scopes": [
    "read",
    "write"
  ]
}
```

<div class="ac-properties">
  <h3>
    <code>authentication</code>
  </h3>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Type</h5>
    </div>
    <div class="aui-item">
      <a href="#authentication">
        <code>Authentication</code>
      </a>
    </div>
  </div>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Required</h5>
    </div>
    <div class="aui-item">
      <span class="aui-lozenge aui-lozenge-subtle aui-lozenge-error">Yes</span>
    </div>
  </div>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Description</h5>
    </div>
    <div class="aui-item">
      <p>Defines the authentication type to use when signing requests between the host application and the connect app.</p>
    </div>
  </div>
  <a id="baseUrl"></a>
  <h3>
    <code>baseUrl</code>
  </h3>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Type</h5>
    </div>
    <div class="aui-item">
      <code>string</code>
      <p></p>
      <code>uri</code>
    </div>
  </div>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Required</h5>
    </div>
    <div class="aui-item">
      <span class="aui-lozenge aui-lozenge-subtle aui-lozenge-error">Yes</span>
    </div>
  </div>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Description</h5>
    </div>
    <div class="aui-item">
      <p>The base url of the remote app, which is used for all communications back to the app instance. Once the app is installed in a product, the app's baseUrl cannot be changed without first uninstalling the app. This is important; choose your baseUrl wisely before making your app public.
      </p>

      <p>The baseUrl must start with <code>https://</code> to ensure that all data is sent securely between our cloud instances and your app.
      </p>
      <p>Note: each app must have a unique baseUrl. If you would like to serve multiple apps from the same host, consider adding a path prefix into the baseUrl.
      </p>
    </div>
  </div>
  <a id="key"></a>
  <h3>
    <code>key</code>
  </h3>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Type</h5>
    </div>
    <div class="aui-item">
      <code>string</code>
      <p></p>
      <code>^[a-zA-Z0-9-._]+$</code>
    </div>
  </div>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Required</h5>
    </div>
    <div class="aui-item">
      <span class="aui-lozenge aui-lozenge-subtle aui-lozenge-error">Yes</span>
    </div>
  </div>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Description</h5>
    </div>
    <div class="aui-item">
      <p>A unique key to identify the app.This key must be &lt;= 80 characters.</p>
    </div>
  </div>
  <a id="apiVersion"></a>
  <h3>
    <code>apiVersion</code>
  </h3>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Type</h5>
    </div>
    <div class="aui-item">
      <code>integer</code>
    </div>
  </div>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Description</h5>
    </div>
    <div class="aui-item">
      <p>The API version is an OPTIONAL integer. If omitted we will infer an API version of 1.</p>
      <p>The intention behind the API version is to allow vendors the ability to beta test a major revision to their Connect app as a private version, and have a seamless transition for those beta customers (and existing customers) once the major revision is launched. </p>
      <p>Vendors can accomplish this by listing a new private version of their app, with a new descriptor hosted at a new URL. </p>

      <p>They use the Atlassian Marketplace's access token facilities to share this version with customers (or for internal use). 
        When this version is ready to be taken live, it can be transitioned from private to public, and all customers will be seamlessly updated.</p>

      <p>It's important to note that this approach allows vendors to create new versions manually, despite the fact that in the common case, the versions are automatically created. This has a few benefits-- for example, it gives vendors the ability to change their descriptor URL if they need to (the descriptor URL will be immutable for existing versions)
      </p>
    </div>
  </div>
  <a id="description"></a>
  <h3>
    <code>description</code>
  </h3>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Type</h5>
    </div>
    <div class="aui-item">
      <code>string</code>
    </div>
  </div>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Description</h5>
    </div>
    <div class="aui-item">
      <p>A human readable description of what the app does. The description will be visible in the <strong>Manage Add-ons</strong> section of the administration console. Provide meaningful and identifying information for the instance administrator.
      </p>
    </div>
  </div>
  <a id="enableLicensing"></a>
  <h3>
    <code>enableLicensing</code>
  </h3>
  <div class="aui-group">
    <div class="aui-item ac-property-key">\
      <h5>Type</h5>
    </div>
    <div class="aui-item">
      <code>boolean</code>
    </div>
  </div>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Defaults to</h5>
    </div>
    <div class="aui-item">
      <code>false</code>
    </div>
  </div>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Description</h5>
    </div>
    <div class="aui-item">
      <p>Whether or not to enable licensing options in the UPM/Marketplace for this app</p>
    </div>
  </div>
  <h3>
    <code>lifecycle</code>
  </h3>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Type</h5>
    </div>
    <div class="aui-item">
      <a href="#lifecycle">
        <code>Lifecycle</code>
      </a>
    </div>
  </div>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Description</h5>
    </div>
    <div class="aui-item">
      <p>Allows the app to register for app lifecycle notifications</p>
    </div>
  </div>
  <a id="links"></a>
  <h3>
    <code>links</code>
  </h3>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Type</h5>
    </div>
    <div class="aui-item">
      <code>object</code>
    </div>
  </div>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Description</h5>
    </div>
    <div class="aui-item">
      <p>A set of links that the app wishes to publish</p>
      <pre>
        {
          "links": {
            "self": "https://app.domain.com/atlassian-connect.json",
            "documentation": "https://app.domain.com/docs"
          }
        }
      </pre>
    </div>
  </div>
  <a id="modules"></a>
  <h3>
    <code>modules</code>
  </h3>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Type</h5>
    </div>
    <div class="aui-item">
      <code>object</code>
    </div>
  </div>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Description</h5>
    </div>
    <div class="aui-item">
      <p>The list of modules this app provides.</p>
    </div>
  </div>
  <a id="name"></a>
  <h3>
    <code>name</code>
  </h3>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Type</h5>
    </div>
    <div class="aui-item">
      <code>string</code>
    </div>
  </div>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Description</h5>
    </div>
    <div class="aui-item">
      <p>The human-readable name of the app</p>
    </div>
  </div>
  <a id="scopes"></a>
  <h3>
    <code>scopes</code>
  </h3>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Type</h5>
    </div>
    <div class="aui-item">[&nbsp;<code>string</code>, … ]
    </div>
  </div>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Description</h5>
    </div>
    <div class="aui-item">
      <p>Set of <a href="https://developer.atlassian.com/cloud/jira/platform/scopes/">scopes</a> requested by this app</p>
      <pre>
        {
          "scopes": [
            "read",
            "write"
          ]
        }
      </pre>
    </div>
  </div>
  <a id="vendor"></a>
  <h3>
    <code>vendor</code>
  </h3>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Type</h5>
    </div>
    <div class="aui-item">
      <a href="#app-vendor"><code>App Vendor</code></a>
    </div>
  </div>
  <div class="aui-group">
    <div class="aui-item ac-property-key">
      <h5>Description</h5>
    </div>
    <div class="aui-item">
      <p>The vendor who is offering the app</p>
    </div>
  </div>
</div>