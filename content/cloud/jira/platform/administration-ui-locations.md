---
title: "Administration UI locations"
platform: cloud
product: jiracloud
category: reference
subcategory: modules
aliases:
- /jiracloud/jira-platform-modules-administration-console-39988333.html
- /jiracloud/jira-platform-modules-administration-console-39988333.md
confluence_id: 39988333
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988333
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988333 
date: "2017-09-11"
---
# Administration UI locations

If your app needs to provide a configuration or administration screen, we recommend adding a link and page to the *Add-ons* section of Jira administration (global settings).

### Available locations

For web sections:

* `admin_plugins_menu`

For web items:

* On this page, you should always define a web section for your app first, even if you only provide one link. This allows your app to have a visually distinct section in the navigation. Therefore, the location for your web item will always be your web section key. See the sample descriptor JSON below.

![global admin overview](../images/global-admin-page.png)

#### Sample descriptor JSON
``` json
...
"modules": {
    "webSections": [
        {
            "key": "example-menu-section",
            "location": "admin_plugins_menu",
            "name": {
                "value": "Example app name"
            }
        }
    ],
    "webItems": [
        {
            "key": "example-section-link",
            "location": "admin_plugins_menu/example-menu-section",
            "name": {
                "value": "Example app link"
            },
            "url": "/example-section-link"
        }
    ]
}
...
```

### Properties

The properties required for this location are the standard ones defined in the documentation for [web sections] and [web items].

[web sections]: /cloud/jira/platform/modules/web-section
[web items]: /cloud/jira/platform/modules/web-item/