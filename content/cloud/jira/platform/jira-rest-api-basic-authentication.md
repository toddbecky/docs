---
title: "Basic auth for REST APIs"
platform: cloud
product: jiracloud
category: devguide
subcategory: security
aliases:
- /jiracloud/jira-rest-api-basic-authentication-39991466.html
- /jiracloud/jira-rest-api-basic-authentication-39991466.md
confluence_id: 39991466
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39991466
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39991466
date: "2018-03-05"
---

# Basic auth for REST APIs

This page shows you how to allow REST clients to authenticate themselves using **[basic authentication](http://en.wikipedia.org/wiki/Basic_access_authentication)** with an [Atlassian account] username and [API token](https://confluence.atlassian.com/x/Vo71Nw). 
This is one of three methods that you can use for authentication against the Jira REST API; the other two are [cookie-based authentication] and [OAuth].

## Before you begin

Have you picked the right authentication method?

{{< include path="docs/content/cloud/jira/platform/temp/authentication-methods.snippet.md">}}


## Overview

Jira's REST API is protected by the same restrictions which are provided via Jira's standard web interface. This means that if you do not log in, you are accessing Jira anonymously. Furthermore, if you log in and do not have permission to view something in Jira, you will not be able to view it using the Jira REST API either.

In most cases, the first step in using the Jira REST API is to authenticate a user account with your Jira site. On this page we will show you a simple example of basic authentication.

## Getting your API token

API tokens are the the recommended method for using basic auth. You can generate an API token for your Atlassian account and use it to authenticate anywhere where you would have used a password. This enhances security because you are not saving your primary account password outside of where you authenticate, you can quickly revoke individual API tokens on a per-use basis, and API tokens will allow you to authenticate even if your Atlassian Cloud organization has two-factor authentication or SAML enabled.

[Follow the instructions here](https://confluence.atlassian.com/x/Vo71Nw) to generate an API token.

<div class="aui-message warning">
    <div class="icon"></div>
    <p class="title">
        <strong>Using passwords with Jira REST API basic authentication</strong>
    </p>
    <br/>
    <p>
    Support for passwords in REST API basic authentication is <a href="/cloud/jira/platform/deprecation-notice-basic-auth-and-cookie-based-auth/">deprecated and will be removed in the future</a>. 
    While the Jira REST API currently accepts your Atlassian account password in basic auth requests,
    we <em>strongly</em> recommend that you use API tokens instead. We expect that support for passwords 
    will be deprecated in the future and advise that all new integrations be created with API tokens.
    </p>
</div>

## Simple example

Most client software provides a simple mechanism for supplying a user name and password (or API token) and will build the required 
authentication headers automatically. For example you can specify the `-u` argument with cURL as follows

``` bash
curl -D- \
   -u fred:freds_api_token \
   -X GET \
   -H "Content-Type: application/json" \
   https://example.com:8081/rest/api/2/issue/createmeta
```

## Supplying basic auth headers

If you need to you may construct and send basic auth headers yourself. To do this you need to perform the following steps:

1. Generate an API token for Jira using your Atlassian Account: https://id.atlassian.com/manage/api-tokens.
2. Build a string of the form `username:api_token`.
3. BASE64 encode the string.
4. Supply an `Authorization` header with content `Basic ` followed by the encoded string. For example, the string `fred:fred` encodes to `ZnJlZDpmcmVk` in base64, so you would make the request as follows:

``` bash
curl -D- \
   -X GET \
   -H "Authorization: Basic ZnJlZDpmcmVk" \
   -H "Content-Type: application/json" \
   "https://your-domain.atlassian.net/rest/api/2/issue/QA-31"
```

### Advanced topics

#### Authentication challenges

Because Jira permits a default level of access to anonymous users, it does not supply a typical authentication challenge. 
Some HTTP client software expect to receive an authentication challenge before they will send an authorization header. 
This means that it may not behave as expected. In this case, you may need to configure it to supply the authorization 
header, as described above, rather than relying on its default mechanism.

#### CAPTCHA

A CAPTCHA is 'triggered' after several consecutive failed log in attempts, after which the user is required to interpret a distorted picture of a word and type that word into a text field with each subsequent log in attempt. If CAPTCHA has been triggered, you cannot use Jira's REST API to authenticate with the Jira site.

You can check this in the error response from Jira -- If there is an `X-Seraph-LoginReason` header with a a value 
of `AUTHENTICATION_DENIED`, this means the application rejected the login without even checking the password. 
This is the most common indication that Jira's CAPTCHA feature has been triggered.

## Related pages

-   [Jira REST API - Cookie-based Authentication][cookie-based authentication]
-   [Jira REST API - OAuth authentication][OAuth]

  [Authentication for apps]: /cloud/jira/platform/authentication-for-apps/
  [cookie-based authentication]: /cloud/jira/platform/jira-rest-api-cookie-based-authentication
  [OAuth]: /cloud/jira/platform/jira-rest-api-oauth-authentication
  [Atlassian account]: https://confluence.atlassian.com/cloud/atlassian-account-for-users-873871199.html